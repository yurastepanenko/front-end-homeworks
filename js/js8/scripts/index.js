"use strict"
let  myInput =  document.getElementById("myInput");
myInput.addEventListener('focusin',focusFunction);
myInput.addEventListener('focusout',outFocusFunction);

function focusFunction() {

  myInput.style.border="1px solid green";
  myInput.style.outline = "none";

    if(Boolean(document.querySelector('.wrap'))){
        document.querySelector('.wrap').remove()
    }
    if(Boolean(document.querySelector('.errorSpan'))){
      document.querySelector('.errorSpan').remove();
    }

}

function outFocusFunction() {
    let div = document.createElement('div');
    div.classList = 'wrap';
    let span = document.createElement('span');
    let button = document.createElement('button');
    let  myInput =  document.getElementById("myInput");
    myInput.style.border="1px solid black";
    span.innerHTML = `Текущая цена: ${myInput.value}`;
    button.innerHTML = `x`;
    button.classList = 'clearPrice';
    myInput.style.color="green";
    //alert(myInput.value);
    if(+myInput.value < 1 ){
        myInput.style.border="1px solid red";
        let errorSpan = document.createElement('span');
        errorSpan.classList = 'errorSpan';
        errorSpan.innerHTML = `Please enter correct price`;
        document.body.after(errorSpan);
    }
    else{
        document.body.before(div);
        div.prepend(span);
        div.append(button);
        console.log( document.querySelector('.clearPrice'));
        document.querySelector('.clearPrice').addEventListener('click', (e) => {
            document.querySelector('.wrap').remove();
            document.getElementById("myInput").value = 0;
        });
    }

}

