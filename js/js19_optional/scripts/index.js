"use strict"
let usersArr = [5,10,15];
let tasksArr = [2,3,4,5,6,7,50,12];
let eDate = '08.07.2020';//Дата дедлайна

function getDedLine(usersArr, tasksArr, eDate){
    let bDate = new Date();//Дата старта
    let speedDay = 0;//Сколько в день можем сделать
    for(let i = 0; i < usersArr.length; i++){
        speedDay += usersArr[i];
    }
    //console.log(speedDay);
    let sumTasks = 0;//Сколько всего надо сделать
    for(let j = 0; j < tasksArr.length; j++){
        sumTasks += tasksArr[j];
    }
    //console.log(sumTasks);
    let useDays = Math.ceil(sumTasks / speedDay); //Сколько дней надо, округляем в большую сторону.
    //console.log(useDays);

    let realDedLine = bDate;// Дата реального дедлайна

    for (let q = 0; q < useDays; q++){
        if (realDedLine.getDay() === 0 || realDedLine.getDay() === 6){
            realDedLine.setDate(realDedLine.getDate() + 1) ;
            useDays++;
            continue;
        }
         realDedLine.setDate(realDedLine.getDate() + 1) ;
    }
    //console.log(realDedLine);

    let diffDay = eDate.substring(0, 2) - realDedLine.getDate(); //Разница в днях дедлайна и реального дедлайна
    //console.log(diffDay);
    if (diffDay >=0) {
        console.log(`Все задачи будут успешно выполнены за ${diffDay} дней до наступления дедлайна!`);
    }
    else{
        console.log(`Команде разработчиков придется потратить дополнительно ${diffDay*8*-1} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
    }

}
getDedLine(usersArr, tasksArr, eDate);


