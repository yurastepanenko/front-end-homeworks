let newObj = new Object();

function copy(obj, newObj) {
    for (let key in obj) {
        if (typeof obj[key] !== "object") {
            newObj[key] = obj[key];
        }
        if (Array.isArray(obj[key])) {
            newObj[key] = obj[key].slice();
        }
        if (typeof obj[key] === "object" && !Array.isArray(obj[key])) {
            newObj[key] = obj[key];
            copy(obj[key], newObj[key]);

        }
    }
}

copy(obj, newObj);

console.log(obj);
console.log(newObj);
