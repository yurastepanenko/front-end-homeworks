"use strict"
let imgs = document.querySelectorAll('.image-to-show');
let stop = null;
/*Кнопка стоп*/
let buttonStop = document.createElement('button');
buttonStop.classList = 'stop';
buttonStop.innerHTML = `Прекратить`;
buttonStop.addEventListener("click", stopSlider);
document.body.after(buttonStop);
/*Кнопка Старт*/
let buttonStart = document.createElement('button');
buttonStart.classList = 'start';
buttonStart.innerHTML = `Возобновить показ`;
buttonStart.addEventListener("click", startSlider);
document.body.after(buttonStart);

let i = 1;

function slider() {
        setTimeout(() => {
            if(stop) return false;
            //удаляем видимый класс у прошлого элемента
            if (Boolean(document.querySelector('.show'))) {
                let del = document.querySelector('.show');
                del.classList.remove('show');
            }
            let img = imgs[i];
            //console.log(`i = ${i}`);

            if (i < imgs.length) {
                //console.log(imgs[i]);
                imgs[i].classList.add('show');
                i++;
                //console.log(`${i}закончилась итерация`);

                slider();
            }
            else{
                if(stop) return false;
                i=1;
                imgs[0].classList.add('show');

                slider();
            }

        }, 3000)

}
function stopSlider(){
    stop = true;
}
function startSlider(){
    stop = false;
    slider();
}
slider();

