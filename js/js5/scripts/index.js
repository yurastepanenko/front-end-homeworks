"use strict"

 function createNewUser() {
     const newUser = {
         name: prompt("Назовите имя пользователя", "Себастьян"),
         lastName: prompt("Назовите фамилию пользователя", "Уточкин"),
         birthday: prompt("Введите дату рождения (текст в формате dd.mm.yyyy)", "08.07.1988"),
         getLogin : function() {
             return  `${this.name[0]}${this.lastName}`.toLowerCase()
            },
         getAge : function() {
             let dateNow = new Date();
             let userDate = +this.birthday.substring(0, 2);
             let userMonth = +this.birthday.substring(3, 5);
             let userYear = +this.birthday.substring(6, 10);
             let age = dateNow.getFullYear()-userYear;

                 if (dateNow.getMonth() +1 < userMonth)
                    {
                     age -= 1;
                 }

                 else if ( dateNow.getMonth()+1 === userMonth  && userDate > dateNow.getDate()){
                     age -= 1;
                 }
             return age;
            },
         getPassword : function(){
             return `${this.name[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.substring(6,10)}`;
         }

         };

     return newUser;
 }
let testUser =  createNewUser();
console.log(testUser);
console.log(testUser.getAge());
console.log(testUser.getPassword());


