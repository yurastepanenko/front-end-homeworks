"use strict"
let person = {
    name  : "Yura",
    texno : {
        pxp :{
            year : 2002,
            value : 2

        },
        js :{
            year :2001,
            value : 5
        },
        html :{
            year :2000,
            value : 7

        },
    },
    kurs : "fe1",
    tel : ['093-123456789','067-123456789']
}
let newPerson = {};
function copyObj(person,newPerson){

    for (let key in person) {
        if (typeof person[key] !== "object") {
            newPerson[key] = person[key];
        }
        if (Array.isArray(person[key])) {
            newPerson[key] = (person[key]);
        }
        if (typeof person[key] === "object" && !Array.isArray(person[key])) {
            newPerson[key] = person[key];
            copyObj(person[key], newPerson[key]);
        }


    }
   return newPerson;
}


 console.log(person);
copyObj (person,newPerson);
console.log(newPerson);