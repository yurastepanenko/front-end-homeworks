"use strict"
let arr = ['hello', 'world', 23, '23', null];
let types = 'string';
function filterBy(arr, types){
    let newArr = arr.filter(item => typeof  item !== types, arr);
    return newArr;
}
console.log(filterBy(arr, types));

/*forEach*/
// let arr = ['hello', 'world', 23, '23', null];
// let types = 'string';
// function filterBy(arr, types){
//     let newArr = [];
//     arr.forEach(function(item, i, arr) {
//         if (typeof  item !== types){
//
//             newArr.push(arr[i]) ;
//         }
//
//     });
//     return newArr;
// }
// console.log(filterBy(arr, types));
